import React, { Component } from 'react';
import './App.css';
import Number from './Companents/Number/Number';

class App extends Component {

    state = {
        numbers: []
    };

    chengeNumber = () => {
        let newNumber = [];
        while (newNumber.length < 5) {
            const random = Math.floor(Math.random()*(36-5)+5);
            if (!newNumber.includes(random)) {
                newNumber.push(random);
            }
        }
        let copyNumber = [...this.state.numbers];
        copyNumber = newNumber;
        copyNumber.sort((a,b) => a - b);

        this.setState({
            numbers: copyNumber
        });
        console.log(this.state.numbers);
    };

  render() {
    return (
      <div className="App">
          <div className='btn-box'>
            <button onClick={this.chengeNumber} className='btn'>New numbers</button>
          </div>
          {this.state.numbers.map((number, index) => {
              return (<Number number={number} key={index}/>)
          })}
      </div>
    );
  }
}



export default App;
