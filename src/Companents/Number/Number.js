import React from 'react';
import './Number.css';

const Number = (props) => {
    return (
        <div className='circle'>
            <span className='txt'>{props.number}</span>
        </div>
    )
};



export default Number;